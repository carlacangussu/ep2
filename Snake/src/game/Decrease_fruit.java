package game;

import java.awt.Color;
import java.awt.Graphics;

public class Decrease_fruit extends Comida {
	
//snake.tamanho_atual = snake.tamanho_inicial; == 5
public Decrease_fruit() {
		
	}
	private int x, y, width, height;
	public Decrease_fruit(int x, int y, int tileSize) {
		this.x = x;
		this.y = y;
		width = tileSize;
		height = tileSize;
		
	}
	//Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit.
	public void tick() {
		
	}
	public void drw(Graphics g) {
		g.setColor(Color.YELLOW);
		g.fillRect(x * width, y *height, width, height);
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
}
