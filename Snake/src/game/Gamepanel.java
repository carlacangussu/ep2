package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class Gamepanel extends JPanel implements Runnable, KeyListener {
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 500, HEIGHT = 500;
	
	private int cont = 0;
	private Thread thread;
	private boolean rodando;
	private boolean right = true, left = false, up = false, down = false;
	
	private BodyPart b;
	private ArrayList<BodyPart> snake;
	
	private Comida comida;
	private ArrayList<Comida> comidas;
	
	private Big_fruit big_fruit;
	private ArrayList<Big_fruit> big_fruits;
	
	private Decrease_fruit decrease;
	private ArrayList<Decrease_fruit> decreases;
	
	private Bomb_fruit bomb;
	private ArrayList<Bomb_fruit> bombs;
	
	private Random r;
	
	private int x =10, y =10, size = 5, score = 0;
	private int ticks = 0;
	
	public Gamepanel() {
		setFocusable(true);
		
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		addKeyListener(this);
		snake =new ArrayList<BodyPart>();
		comidas = new ArrayList<Comida>();
		big_fruits = new ArrayList<Big_fruit>();
		decreases = new ArrayList<Decrease_fruit>();
		bombs = new ArrayList<Bomb_fruit>();
		
		r = new Random();
		start();
	}
	
	public void start() {
		rodando = true;
		thread = new Thread(this);
		thread.start();
	}
	public void stop() {
		rodando = false;
		try {
			thread.join();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void tick() {
		
		if(snake.size() == 0) {
			b = new BodyPart(x, y, 10);
			snake.add(b);
		}
		ticks++;
		if(ticks > 4500000) {
			if(right) x++;
			if(left) x--;
			if(up) y--;
			if(down) y++;
			
			ticks = 0;
			b =new BodyPart(x, y,10);
			snake.add(b);
			if(snake.size() > size) {
				snake.remove(0);
			}
			if(comidas.size() == 0) {
				int x = r.nextInt(49);
				int y = r.nextInt(49);
				
				comida = new Comida(x, y, 10);
				comidas.add(comida);
			}
			for(int i = 0; i< comidas.size(); i++) {
				if(x == comidas.get(i).getX() && y == comidas.get(i).getY()) {
					size++;
					score++;
					comidas.remove(i);
					i++;
				}			
			}
			if(big_fruits.size() == 0) {
				int x = r.nextInt(49);
				int y = r.nextInt(49);
				
				big_fruit = new Big_fruit(x, y, 10);
				big_fruits.add(big_fruit);
			}
			for(int i = 0; i< big_fruits.size(); i++) {
				if(x == big_fruits.get(i).getX() && y == big_fruits.get(i).getY()) {
					size++;
					score = score + 2;
					big_fruits.remove(i);
					i++;
				}		
			}
			if(decreases.size() == 0) {
				int x = r.nextInt(49);
				int y = r.nextInt(49);
				
				decrease = new Decrease_fruit(x, y, 10);
				decreases.add(decrease);
			}
			for(int i = 0; i< decreases.size(); i++) {
				if(x == decreases.get(i).getX() && y == decreases.get(i).getY()) {
					size = 5;
					score++;
					decreases.remove(i);
					i++;
				}		
			}
			if(bombs.size() == 0) {
				int x = r.nextInt(49);
				int y = r.nextInt(49);
				
				bomb = new Bomb_fruit(x, y, 10);
				bombs.add(bomb);
			}
			for(int i = 0; i< bombs.size(); i++) {
				if(x == bombs.get(i).getX() && y == bombs.get(i).getY()) {
					bombs.remove(i);
					setCont(getCont() + 1);
				}	   
				
								}		
			}
		}
	
	public void paint(Graphics g) {
		g.clearRect(0, 0, WIDTH, HEIGHT);
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		for(int i = 0; i< WIDTH/10; i++) {
			g.drawLine(i * 10, 0, i *10, HEIGHT);
		}
		for(int i = 0; i< HEIGHT/10; i++) {
			g.drawLine(0,i * 10, HEIGHT, i *10);
		}
		for(int i =0; i<snake.size(); i++) {
			snake.get(i).drw(g);
		}
		
		for(int i =0; i<comidas.size(); i++) {
			comidas.get(i).drw(g);
	
			
		}
		for(int i =0; i<big_fruits.size(); i++) {
			big_fruits.get(i).drw(g);
		
		}
		
		for(int i =0; i < decreases.size(); i++) {
			decreases.get(i).drw(g);
		}
		for(int i =0; i < bombs.size(); i++) {
			bombs.get(i).drw(g);
		}
		
		// colisão com o corpo
		for(int i  = 0; i < snake.size(); i++) {
			if(x == snake.get(i).getX() && y == snake.get(i).getY()) {
				if(i != snake.size()- 1) {
					String msg = "Game Over \nScore: "+ score;
			        Font small = new Font("Helvetica", Font.BOLD, 14);
			        FontMetrics metr = getFontMetrics(small);

			        g.setColor(Color.white);
			        g.setFont(small);
			        g.drawString(msg, (WIDTH - metr.stringWidth(msg)) / 2, HEIGHT / 2);
			        		        
					stop();
				}
					
			}
		}
		//colisão com a borda
		if(x < 0 || x > 49 || y < 0 || y > 49) {
			String msg = "Game Over \nScore: "+ score;
	        Font small = new Font("Helvetica", Font.BOLD, 14);
	        FontMetrics metr = getFontMetrics(small);

	        g.setColor(Color.white);
	        g.setFont(small);
	        g.drawString(msg, (WIDTH - metr.stringWidth(msg)) / 2, HEIGHT / 2);
	        		        
			stop();
		}
		if(cont == 1) {
			String msg = "Game Over \nScore: "+ score;
	        Font small = new Font("Helvetica", Font.BOLD, 14);
	        FontMetrics metr = getFontMetrics(small);

	        g.setColor(Color.white);
	        g.setFont(small);
	        g.drawString(msg, (WIDTH - metr.stringWidth(msg)) / 2, HEIGHT / 2);
	        		        
			stop();
		}
	}

	@Override
	public void run() {
		while(rodando) {
			tick();
			repaint();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if(key == KeyEvent.VK_RIGHT && !left) {
			right = true;
			up = false;
			down = false;
		}
		if( key == KeyEvent.VK_LEFT && !right) {
			left = true;
			up = false;
			down = false;
		}
		if(key == KeyEvent.VK_UP && !down) {
			right = false;
			left = false;
			up = true;
		}
		if(key == KeyEvent.VK_DOWN && !up) {
			right = false;
			left = false;
			down = true;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

}
